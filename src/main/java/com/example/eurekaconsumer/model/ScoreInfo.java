package com.example.eurekaconsumer.model;

public class ScoreInfo {
    private String userId;
    private int score;

    public String getUserId() {

        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
