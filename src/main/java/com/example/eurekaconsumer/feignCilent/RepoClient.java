package com.example.eurekaconsumer.feignCilent;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 创建一个feign客户端接口定义
 */
@FeignClient("eureka-client") // 指定服务名称
public interface RepoClient {
    @PostMapping("/productRepo")  // 使用springmvc 的注解就可以了
    public String productRepo(@RequestParam("productId") String productId,@RequestParam("amount") int amount);
}
