package com.example.eurekaconsumer;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.client.RestTemplate;

@EnableDiscoveryClient // 将当期应用加入到服务治理体系中
@SpringBootApplication
@EnableTransactionManagement
@MapperScan("com.example.eurekaconsumer.dao")
@EnableFeignClients // 开启扫描feign 客户端
@EnableCircuitBreaker  // 或@EnableHystrix 或者 @SpringCloudApplication 这个注解包括了 服务发现和服务熔断
public class EurekaConsumerApplication {
    @Bean
    @LoadBalanced  //ｒｉｂｂｏｎ　的组件
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    public static void main(String[] args) {
       // SpringApplication.run(EurekaConsumerApplication.class, args);
        new SpringApplicationBuilder(EurekaConsumerApplication.class).web(true).run(args);

    }

}
