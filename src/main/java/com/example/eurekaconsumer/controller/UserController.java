package com.example.eurekaconsumer.controller;

import com.example.eurekaconsumer.dao.OrderMapper;
import com.example.eurekaconsumer.dao.ProductMapper;
import com.example.eurekaconsumer.dao.UserMapper;
import com.example.eurekaconsumer.model.OrderInfo;
import com.example.eurekaconsumer.model.ProductInfo;
import com.example.eurekaconsumer.model.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {
    @Resource
    UserMapper userMapper;
    @Resource
    OrderMapper orderMapper;
    @Resource
    ProductMapper productMapper;
    @RequestMapping(value = "/login",method = RequestMethod.GET)
    public String login(Model model, HttpServletRequest request, HttpServletResponse response){
        return "login";
    }
    @RequestMapping(value = "/doLogin",method = RequestMethod.POST)
    public String doLogin(Model model, @RequestParam("userName") String userName,@RequestParam("password") String password,HttpServletRequest request, HttpServletResponse response){
        UserInfo userInfo = new UserInfo();
        userInfo.setUserName(userName);
        userInfo.setPassword(password);
        int count = userMapper.selectByPassword(userInfo);
        String result="login";
        if(count>0){
            result = "index";
            model.addAttribute("userName",userName);
            List<ProductInfo> productInfoList = productMapper.selectAll();
            model.addAttribute("productInfoList",productInfoList);
        }else{
            System.out.println("密码错误");
        }
        return result;
    }

}
