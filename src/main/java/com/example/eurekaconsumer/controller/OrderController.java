package com.example.eurekaconsumer.controller;

import com.example.eurekaconsumer.common.IdUtil;
import com.example.eurekaconsumer.dao.OrderMapper;
import com.example.eurekaconsumer.dao.ProductMapper;
import com.example.eurekaconsumer.dao.ScoreMapper;
import com.example.eurekaconsumer.dao.UserMapper;
import com.example.eurekaconsumer.feignCilent.RepoClient;
import com.example.eurekaconsumer.model.OrderInfo;
import com.example.eurekaconsumer.model.ProductInfo;
import com.example.eurekaconsumer.model.ScoreInfo;
import com.example.eurekaconsumer.service.ConsumerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.*;

@Controller
@RequestMapping(value = "/order")
public class OrderController {
    @Resource
    UserMapper userMapper;
    @Resource
    OrderMapper orderMapper;
    @Resource
    ProductMapper productMapper;

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    RepoClient repoClient;

    @Autowired
    ConsumerService consumerService;
    @Autowired
    IdUtil idUtil;

    @Resource
    ScoreMapper scoreMapper;

    @RequestMapping(value = "/deal",method = RequestMethod.POST)
    @Transactional
    public String dealOrder(Model model, @RequestParam("productId") String productId,
               @RequestParam("price") String price,@RequestParam("sum") String sum){
        // 第一个阶段：这个地址很特殊，没有端口和ip，而是要调用服务的名称，因为ribbon 有一个拦截器，会用名称选择服务，并且用真是地址和端口替换掉这个
      // Object obj = restTemplate.getForObject("http://eureka-client/productRepo",String.class);

        // 第二个阶段：使用feign 调用 ，比上边的更好，更简单
     //   String obj = repoClient.productRepo();
      //  System.out.println("调用 http://eureka-client/productRepo 返回结果："+obj);
        // 第三个阶段，使用Hystrix 服务降级，如果超时就 执行自己定义的
        // 断路器，三个因素：快照时间窗，请求总数下限，错误百分比下限

        Map<String,String> map = new HashMap<>();
        map.put("productId",productId);
        map.put("sum",sum);
        // 步骤1：生成订单信息，创建最新订单
        OrderInfo orderInfo = new OrderInfo();
        orderInfo.setOrderId(idUtil.createOrderId());
        orderInfo.setMoney(Double.parseDouble(price));
        orderInfo.setAmount(Integer.parseInt(sum));
        orderInfo.setCreateTime(new Date());
        orderInfo.setStatus(0);
        orderInfo.setProductId(productId);
        orderInfo.setUserId("1");
        orderMapper.insert(orderInfo);
        List<OrderInfo> orderInfoList = orderMapper.selectOrderInfo(); // 查询订单
        long start = System.currentTimeMillis();
        // 步骤2 ： 调用库存服务，更新库存信
        Object obj = consumerService.productRepo(productId,1);
        long end = System.currentTimeMillis();
        System.out.println(" 调用 库存服务返回："+obj);
        // 步骤3 调用积分服务，更新积分信息（待添加）
        int score = scoreMapper.getScoreById("1");
        ScoreInfo scoreInfo = new ScoreInfo();
        scoreInfo.setUserId("1");
        scoreInfo.setScore(score+Integer.parseInt(price));
        scoreMapper.update(scoreInfo);
        score = scoreMapper.getScoreById("1");
        // 处理需求购买一个苹果手机，需要 更新库存信息和 个人积分信息
        List<ProductInfo> productInfoList = productMapper.selectAll();
        model.addAttribute("productInfoList",productInfoList);
        model.addAttribute("orderInfoList",orderInfoList);
        model.addAttribute("socre",score);
        model.addAttribute("amount",obj);
        return "index";
    }
}
