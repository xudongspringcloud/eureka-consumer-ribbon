package com.example.eurekaconsumer.service;

import com.example.eurekaconsumer.feignCilent.RepoClient;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ConsumerService {
    @Autowired
    RestTemplate restTemplate;
    @Autowired
    RepoClient repoClient;
    // 服务降级和线程隔离。这个是一体的
    @HystrixCommand(fallbackMethod = "fallback")
    public Object productRepo(String productId,int amount){
        String obj =  repoClient.productRepo(productId,amount);
        return obj;
    }
    public String fallback(String productId,int amount) {
        return "fallback";
    }
}
