package com.example.eurekaconsumer.dao;

import com.example.eurekaconsumer.model.ScoreInfo;

public interface ScoreMapper {
    int update(ScoreInfo scoreInfo);
    int getScoreById(String userId);
}
