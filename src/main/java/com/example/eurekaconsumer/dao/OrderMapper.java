package com.example.eurekaconsumer.dao;

import com.example.eurekaconsumer.model.OrderInfo;

import java.util.List;

public interface OrderMapper {
    List<OrderInfo> selectAll();
    int count();
    /**
     * 最新订单，显示前五条
     * @return
     */
    List<OrderInfo> selectOrderInfo();

    /**
     * 插入一条订单信息
     * @param orderInfo
     * @return
     */
    int insert(OrderInfo orderInfo);
}
