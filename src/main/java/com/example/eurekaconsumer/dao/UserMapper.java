package com.example.eurekaconsumer.dao;

import com.example.eurekaconsumer.model.UserInfo;

import java.util.List;

public interface UserMapper {
    List<UserInfo> selectAll();
    int selectByPassword(UserInfo userInfo);
}
