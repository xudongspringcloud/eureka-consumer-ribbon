package com.example.eurekaconsumer.common;

import com.example.eurekaconsumer.dao.OrderMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class IdUtil {
    @SuppressWarnings("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    OrderMapper orderMapper;

    public String createOrderId(){
        return String.valueOf(orderMapper.count()+1);
    }
}
